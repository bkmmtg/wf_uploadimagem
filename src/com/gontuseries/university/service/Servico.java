package com.gontuseries.university.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.gontuseries.university.connection.Conexao;
import com.gontuseries.university.model.ArquivoReembolso;

public class Servico {
	
	private Conexao conexao;
	private PreparedStatement stmt;
	
	public Servico() {
		
		stmt = null;
		conexao = new Conexao();
		
	}
	
	public String salvarArquivo( ArquivoReembolso arquivoReembolso, File imagem) {
		
		String sql = "INSERT INTO arquivoreembolso(idReembolso,numeroReembolso,arquivo,extensao) VALUES(?,?,?,?)";
			
		try {
			InputStream inputStream = new FileInputStream(imagem);

			stmt = conexao.prepareStatement(sql);

			stmt.setString(1, arquivoReembolso.getIdReembolso());
			stmt.setString(2,arquivoReembolso.getNumeroReembolso());
			stmt.setBinaryStream(3, inputStream, (int) imagem.length());
			stmt.setString(4,arquivoReembolso.getExtensao());
			stmt.executeUpdate();

			System.out.println("Imagem Salva Com Sucesso");

		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}catch (NullPointerException e) {
			e.printStackTrace();
		}
		
		return arquivoReembolso.getIdReembolso();
		
	}
	
	public Integer obterNumeroArquivoPorId( String id ) {
		
		Integer numeroArquivo = 0;
		String sql = "SELECT MAX(numeroArquivos) AS ultimoNumeroArquivo FROM arquivoreembolso WHERE idReembolso = '" + id + "'";
		
		try {
			stmt = conexao.prepareStatement(sql);
			
			ResultSet result = stmt.executeQuery();
			
			while( result.next() ) {
				numeroArquivo = result.getInt("ultimoNumeroArquivo");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(NullPointerException e) {
			e.printStackTrace();
		}
		
		return numeroArquivo + 1;
		
	}
	
	public String atualizarNumeroArquivo( ArquivoReembolso arquivoReembolso ) {
		
		String sql = "UPDATE arquivoreembolso set numeroArquivos=numeroArquivos+1 where idReembolso=(?)";
		
		try {
			stmt = conexao.prepareStatement(sql);
			
			stmt.setString(1,arquivoReembolso.getIdReembolso());
			stmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(NullPointerException e) {
			e.printStackTrace();
		}
		
		
		return arquivoReembolso.getIdReembolso();
		
	}

}
