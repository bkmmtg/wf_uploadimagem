package com.gontuseries.university.model;

public class ArquivoReembolso {

	private String idReembolso;
	
	private String numeroReembolso;
	
	private Integer numeroArquivo;
	
	private String extensao;
	
	public ArquivoReembolso( String idReembolso, String numeroReembolso ) {
		
		this.idReembolso = idReembolso;
		this.numeroReembolso = numeroReembolso;
		
	}

	public String getIdReembolso() {
		return idReembolso;
	}

	public void setIdReembolso(String idReembolso) {
		this.idReembolso = idReembolso;
	}

	public String getNumeroReembolso() {
		return numeroReembolso;
	}

	public void setNumeroReembolso(String numeroReembolso) {
		this.numeroReembolso = numeroReembolso;
	}

	public Integer getNumeroArquivo() {
		return numeroArquivo;
	}

	public void setNumeroArquivo(Integer numeroArquivo) {
		this.numeroArquivo = numeroArquivo;
	}

	public String getExtensao() {
		return extensao;
	}

	public void setExtensao(String extensao) {
		this.extensao = extensao;
	}
	
}
