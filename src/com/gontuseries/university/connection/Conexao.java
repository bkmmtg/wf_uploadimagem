package com.gontuseries.university.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Conexao {
	
	private Connection connection;

	public Conexao() {
		
		//String jdbcUrl = "jdbc:mysql://localhost/timesheet?useUnicode=true&characterEncoding=utf-8";
		String jdbcUrl = "jdbc:mysql://localhost:3307/reembolso";
		String username = "root";
		String password = "";
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(jdbcUrl, username, password);
		} catch (SQLException e) {
			System.out.println("Erro na conexao");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} 
		
	}

	public PreparedStatement prepareStatement(String sql) throws SQLException, NullPointerException {
		return connection.prepareStatement(sql);
	}
	
	public void fecharConexao() throws SQLException {
		connection.close();
	}
	
}
